﻿using EfRelationships.Models;
using System.Data.Entity;

namespace EfRelationships
{
    class BloggingContext : DbContext
    {
        public DbSet<Blog> Blobs { get; set; }
        public DbSet<Post> Posts { get; set; }

        public BloggingContext()
        {
            Database.SetInitializer<BloggingContext>(new DropCreateDatabaseAlways<BloggingContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NaturalBlog>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("NaturalBlog");
            });
            modelBuilder.Entity<SpecialBlog>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("SpecialBlog");
            });
            base.OnModelCreating(modelBuilder);

        }
    }
}
