﻿using EfRelationships.Models;
using System;
using System.Collections.Generic;

namespace EfRelationships
{
    class Program
    {
        static void Main(string[] args)
        {
            using (BloggingContext context = new BloggingContext())
            {

                context.Blobs.AddRange(
                    new List<Blog> {
                        new NaturalBlog{ Name = "Natural Blog", Forest = "Amazon"},
                        new SpecialBlog{ Name = "Special Blog", BlogCategory = "Special"}
                    }
                    );
                context.SaveChanges();
            }
            System.Console.WriteLine("Relationship created in DB ");
            Console.Read();
        }
    }
}
