﻿using System.Collections.Generic;
using System.Data.Entity;

namespace _005_RandomTopics.DBInitializers
{
    class CustomDatabaseinitializer : IDatabaseInitializer<BloggingContext>
    {
        public void InitializeDatabase(BloggingContext context)
        {
            if (context.Database.Exists())
            {
                context.Database.Delete();
            }
            context.Database.Create();
            context.Blogs.AddRange(
                new List<Blog> {
                    new Blog{ Name = "First Blog"},
                    new Blog{ Name = "Second Blog"}
                }
                );
            context.SaveChanges();
        }
    }
}
