﻿using System.Collections.Generic;
using System.Data.Entity;

namespace _005_RandomTopics.DBInitializers
{
    class CreateDbWhenModelChanges : DropCreateDatabaseIfModelChanges<BloggingContext>
    {
        protected override void Seed(BloggingContext context)
        {
            base.Seed(context);
            context.Blogs.AddRange(
                new List<Blog> {
                    new Blog{ Name = "First Blog"},
                    new Blog  { Name = "Second Blog"}
                }
            );
        }
    }
}
