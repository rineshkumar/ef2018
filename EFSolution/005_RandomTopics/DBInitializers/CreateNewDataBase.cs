﻿using System.Data.Entity;

namespace _005_RandomTopics.DBInitializers
{
    class CreateNewDataBase : CreateDatabaseIfNotExists<BloggingContext>
    {
        protected override void Seed(BloggingContext context)
        {
            base.Seed(context);
        }
    }
}
