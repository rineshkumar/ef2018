﻿using System.Collections.Generic;
using System.Data.Entity;

namespace _005_RandomTopics.DBInitializers
{
    class AlwaysCreateNewDatabase : DropCreateDatabaseAlways<BloggingContext>
    {
        protected override void Seed(BloggingContext context)
        {
            base.Seed(context);
            context.Blogs.AddRange(
                new List<Blog> {
                    new Blog{ Name = "Third Blog"},
                    new Blog{ Name = "Fourth Blog"}
                }
            );
        }
    }
}
