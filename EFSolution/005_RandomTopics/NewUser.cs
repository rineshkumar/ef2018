namespace _005_RandomTopics
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NewUser
    {
        [Key]
        [StringLength(50)]
        public string Username { get; set; }

        public string DisplayName { get; set; }
    }
}
