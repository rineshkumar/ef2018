﻿using _005_RandomTopics.DBInitializers;
using System;
using System.Data.Entity;

namespace _005_RandomTopics
{
    class Program
    {
        static void Main(string[] args)
        {
            // UseCreateDatabaseIfNotExistsInitializer();
            //UseAlwaysCreateNewInitializer();
            UseCreateDbWhenModelChangesInitializer();
            UseCustomDbInitializer();
            //using (var context = new BloggingContext())
            //{
            //    context.Blogs.AddRange(
            //        new List<Blog> {
            //            new Blog{ Name = "Fifth"},
            //            new Blog{ Name = "Sixth"}
            //        }

            //        );
            //    context.SaveChanges();
            //}
            using (var context = new BloggingContext())
            {

                foreach (var blog in context.Blogs)
                {
                    System.Console.WriteLine(blog.Name);
                }
            }
            Console.ReadLine();
        }

        private static void UseCustomDbInitializer()
        {
            Database.SetInitializer<BloggingContext>(new CustomDatabaseinitializer());
        }

        private static void UseCreateDbWhenModelChangesInitializer()
        {
            Database.SetInitializer<BloggingContext>(new CreateDbWhenModelChanges());
        }

        private static void UseAlwaysCreateNewInitializer()
        {
            Database.SetInitializer<BloggingContext>(new AlwaysCreateNewDatabase());
        }

        private static void UseCreateDatabaseIfNotExistsInitializer()
        {
            Database.SetInitializer<BloggingContext>(new CreateNewDataBase());
        }
    }
}
