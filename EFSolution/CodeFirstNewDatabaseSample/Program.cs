﻿using CodeFirstNewDatabaseSample.Model;
using System;

namespace CodeFirstNewDatabaseSample
{
    class Program
    {
        static void Main(string[] args)
        {
            AddNewBlog();
        }

        private static void AddNewBlog()
        {
            Console.Write("Enter a name for a new Blog: ");
            var name = Console.ReadLine();
            var blog = new Blog { Name = name };
            using (var context = new BloggingContext())
            {
                context.Blogs.Add(blog);
                context.SaveChangesAsync().Wait();

                Console.WriteLine("List of blogs in database    ");
                var blogList = context.Blogs;
                foreach (var item in blogList)
                {
                    Console.WriteLine(item.Name);
                }
            }
            Console.ReadLine();

        }
    }
}
