﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UOWProject.DAL;

namespace UOWProject.Model
{
    public class StudentRepository : IStudentRepository, IDisposable
    {
        public SchoolContext schoolContext { get; set; }

        public StudentRepository(SchoolContext schoolContext)
        {
            this.schoolContext = schoolContext;
        }

        public void CreateStudent(Student student)
        {
            schoolContext.Students.Add(student);
        }

        public void DeleteStudent(int studentId)
        {
            var student = schoolContext.Students.Find(studentId);
            schoolContext.Students.Remove(student);
        }


        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
        bool currentObjectDisplosed = false;

        protected virtual void Dispose(bool calledByDisposeMethod)
        {
            if (!currentObjectDisplosed)
            {
                if (calledByDisposeMethod)
                {
                    if (schoolContext != null)
                    {
                        schoolContext.Dispose();
                    }

                }
            }
            currentObjectDisplosed = true;
        }

        public List<Student> GetAllStudents()
        {
            return schoolContext.Students.ToList();
        }

        public Student GetStudent(int StudentId)
        {
            return schoolContext.Students.Find(StudentId);
        }

        public void Save()
        {
            schoolContext.SaveChanges();
        }

        public void UpdateStudent(Student student)
        {
            schoolContext.Entry(student).State = EntityState.Modified;
        }
    }
}
