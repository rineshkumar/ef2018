﻿using System.Collections.Generic;

namespace ContosoUniversity.Models
{
    public class Course
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public List<Student> Students { get; set; }
    }
}