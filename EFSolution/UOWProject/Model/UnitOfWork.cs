﻿using ContosoUniversity.Models;
using System;
using UOWProject.DAL;

namespace UOWProject.Model
{
    class UnitOfWork : IDisposable, IUnitOfWork
    {

        //https://www.danylkoweb.com/Blog/a-better-entity-framework-unit-of-work-pattern-DD
        SchoolContext Context { get; set; }
        private RepositoryImpl<Student> _schoolRepository;
        private RepositoryImpl<Course> _courseRepository;

        public UnitOfWork(SchoolContext context)
        {
            Context = context;
        }

        public UnitOfWork() : this(new SchoolContext())
        {
        }

        public RepositoryImpl<Course> CourseRepository
        {
            get
            {
                if (_courseRepository == null)
                    _courseRepository = new RepositoryImpl<Course>(Context);
                return _courseRepository;
            }

        }

        public RepositoryImpl<Student> StudentRepository
        {
            get
            {
                if (_schoolRepository == null)
                    _schoolRepository = new RepositoryImpl<Student>(Context);
                return _schoolRepository;
            }

        }



        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        bool currentObjectDisposed = false;
        private void Dispose(bool calledFromDisposed)
        {
            if (!currentObjectDisposed)
            {
                if (calledFromDisposed)
                {
                    Context.Dispose();
                }
            }
            currentObjectDisposed = true;
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
