﻿using System.Collections.Generic;

namespace UOWProject.Model
{
    public class ManySide
    {
        public int ManySideId { get; set; }
        public string ManySideProperty { get; set; }
        public OneSide OneSideItem { get; set; }
        public List<OneSide> ManyOneSideItems { get; set; }
    }
}
