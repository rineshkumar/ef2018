﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContosoUniversity.Models
{
    public class Blog
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string BloggerName { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public BlogDetails BlogDetail { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

}