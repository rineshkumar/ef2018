﻿namespace UOWProject.Model
{
    public class OptionalSide
    {
        public int OptionalSideId { get; set; }
        public OneSide OneSideItem { get; set; }
    }
}
