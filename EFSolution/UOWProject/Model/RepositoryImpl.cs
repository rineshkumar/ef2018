﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UOWProject.DAL;

namespace UOWProject.Model
{
    class RepositoryImpl<T> : IRepository<T> where T : class
    {
        public SchoolContext Context { get; set; }
        public DbSet<T> DbSet { get; set; }

        public RepositoryImpl(SchoolContext context)
        {
            Context = context;
            this.DbSet = Context.Set<T>();
        }

        public void Create(T entity)
        {
            DbSet.Add(entity);
        }

        public void Delete(int Id)
        {
            var entity = this.DbSet.Find(Id);
            Delete(entity);
        }

        private void Delete(T entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }

        public T Get(int Id)
        {
            return DbSet.Find(Id);
        }

        public List<T> GetAll()
        {
            return DbSet.ToList();
        }



        public void Update(T entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }
    }
}
