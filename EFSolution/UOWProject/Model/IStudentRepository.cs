﻿using ContosoUniversity.Models;
using System;
using System.Collections.Generic;

namespace UOWProject.Model
{
    interface IStudentRepository : IDisposable
    {
        //CRD operations 
        //Create 
        void CreateStudent(Student student);
        //Read
        Student GetStudent(int StudentId);
        List<Student> GetAllStudents();
        //Udpate
        void UpdateStudent(Student student);
        //delete student 
        void DeleteStudent(int studentId);
        //Save
        void Save();

    }
}
