﻿using System.Collections.Generic;

namespace UOWProject.Model
{
    interface IRepository<T> where T : class
    {
        void Create(T entity);
        void Delete(int Id);
        List<T> GetAll();
        T Get(int Id);
        //   void Save();
        void Update(T entity);
    }
}