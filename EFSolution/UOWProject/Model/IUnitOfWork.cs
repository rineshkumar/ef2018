﻿namespace UOWProject.Model
{
    interface IUnitOfWork
    {
        void Save();
    }
}