﻿using System.Collections.Generic;

namespace UOWProject.Model
{
    public class OneSide
    {
        public int Id { get; set; }
        public int OneSideId2 { get; set; }
        public string OneSideProperty { get; set; }
        public int IgnoredProperty { get; set; }
        public List<ManySide> ManySideItems { get; set; }
        public OptionalSide OptionalSideItem { get; set; }
    }
}
