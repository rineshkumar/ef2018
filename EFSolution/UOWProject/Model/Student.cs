﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContosoUniversity.Models
{
    public class Student
    {
        public int StudentID { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public string Language { get; set; }
        // public string MotherLand { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public List<Course> Courses { get; set; }
    }
}