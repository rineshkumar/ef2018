﻿using ContosoUniversity.Models;
using System;
using UOWProject.DAL;

namespace UOWProject
{
    class Program
    {
        static void Main(string[] args)
        {

            //Console.WriteLine("Press enter to continue ");
            //Console.ReadLine();
            //CreateBlog();

            ListBlogsUsingSqlQuery();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ListBlogsUsingSqlQuery()
        {
            using (var context = new SchoolContext())
            {
                //var blogs = context.Blogs.SqlQuery("Select * from College.Blog").ToListAsync().Result;
                //  var blogs = context.Blogs.SqlQuery("dbo.GetBlogs").ToListAsync().Result;
                //var blogId = 1;
                //var blogs = context.Blogs.SqlQuery("dbo.GetBlogs @p0", blogId).ToListAsync().Result;
                //var blogNames = context.Database.SqlQuery<string>("select name from blog").ToList();
                //foreach (var item in blogNames)
                //{
                //    Console.WriteLine(blogNames);
                //}
                context.Database.ExecuteSqlCommand("update blogs set Name = 'Another Name' where BlogId = 1 ");

            }
        }

        private static void CreateBlog()
        {
            using (var context = new SchoolContext())
            {
                context.Blogs.Add(
                    new Blog { Title = "First Blog", BlogDetail = new BlogDetails() }
                );

                context.SaveChanges();
            }
        }
    }
}
