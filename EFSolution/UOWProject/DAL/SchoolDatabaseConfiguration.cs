﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace UOWProject.DAL
{
    class SchoolDatabaseConfiguration : DbConfiguration
    {
        protected internal SchoolDatabaseConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient",
                () => new SqlAzureExecutionStrategy());
        }

    }
}
