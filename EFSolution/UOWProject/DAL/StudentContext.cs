﻿using ContosoUniversity.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using UOWProject.Model;

namespace UOWProject.DAL
{
    [DbConfigurationType(typeof(SchoolDatabaseConfiguration))]
    public class SchoolContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        // public DbSet<Course> Courses { get; set; }

        public DbSet<OneSide> OneSide { get; set; }
        public DbSet<ManySide> ManySide { get; set; }

        public DbSet<RequiredSide> RequiredSide { get; set; }
        public DbSet<OptionalSide> OptionalSide { get; set; }

        public DbSet<Blog> Blogs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //  this.Database.Log = s => FileLogger.Log(s);
            // DbInterception.Add(new NLogCommandInterceptor());
            Configuration.LazyLoadingEnabled = false;
            base.OnModelCreating(modelBuilder);
            //Database.SetInitializer<SchoolContext>(new DropCreateDatabaseAlways<SchoolContext>());

            //On schema level
            modelBuilder.HasDefaultSchema("College");
            modelBuilder.Ignore<RequiredSide>();
            /*
             * on Entity Level 
             * 1. Which Table to use . 
             * 2. Which Properties to use as primary key 
             * 3. Which property to ignore . 
             * 4. Which property on which index needs to be created. 
             * 5. generate stored procedues for create / update / delete 
             * 6. Relationships 
             */
            var entity = modelBuilder.Entity<OneSide>()
            .ToTable("OneSideTable") // What table to use 
            .HasKey(x => x.Id) //Which columns to use as primary key 
            .Ignore(x => x.IgnoredProperty) //Which columns to ignore 
            .MapToStoredProcedures();

            //One to one relationship - secodary Table - Foreign Key null 
            //.HasOptional<OptionalSide>(x => x.OptionalSideItem)
            //.WithRequired(x => x.OneSideItem);

            //one to one relationship - secondary table - Foreign key - not null 
            entity.HasRequired<OptionalSide>(x => x.OptionalSideItem)
            .WithRequiredPrincipal(x => x.OneSideItem)
            .WillCascadeOnDelete();
            //one to many relationship - secondary table - Foreign key null
            //entity
            //    .HasMany<ManySide>(x => x.ManySideItems)
            //    .WithOptional(x => x.OneSideItem);
            //    .WillCascadeOnDelete();
            //one to many relationship - secondary table - Foreign key not null 
            //entity
            //    .HasMany<ManySide>(x => x.ManySideItems)
            //    .WithRequired(x => x.OneSideItem)
            //    .WillCascadeOnDelete();
            //Many to many - With custom mapping table 
            entity
                .HasMany<ManySide>(x => x.ManySideItems)
                .WithMany(x => x.ManyOneSideItems).Map(m =>
                {
                    m
                    .ToTable("OneManyMapping")
                    .MapLeftKey("OneKey")
                    .MapRightKey("ManyKey");
                });


            //On Property Level 
            /*
             * 1. Name of the column to be used in DB 
             * 2. Type of the column to be used in DB 
             * 3. order of the column to be used in DB 
             * 4. Is the column required 
             * 5. Is the column optional . 
             * 6. Is it meant for concurrency control 
             * 7. How is it generated in DB 
             */

            modelBuilder.Entity<Student>()

                    .Property(s => s.FirstMidName)

                    .HasColumnName("FirstName")
                    .HasColumnType("varchar")
                    .HasColumnOrder(3)

                    .IsOptional()
                    .IsRequired()

                    .IsConcurrencyToken()

                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
